import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(Home());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Material App', debugShowCheckedModeBanner: false, home: Home());
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool colorBool;
  @override
  void initState() {
    super.initState();
    colorBool = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Material App Bar'),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              colorBool = !colorBool;
            });
          },
          child: Icon(
            Icons.ac_unit,
          )),
      body: Row(
        children: [
          Container(
            width: 100,
            height: 100,
            color: colorBool ? Colors.red : Colors.yellow,
          ),
          Container(
            width: 100,
            height: 100,
            color: !colorBool ? Colors.red : Colors.yellow,
          ),
          SizedBox(
            width: 20,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
          )
        ],
      ),
    );
  }
}
